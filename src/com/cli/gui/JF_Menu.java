package com.cli.gui;
import javax.swing.JDialog;

public class JF_Menu extends javax.swing.JFrame {

    public String usu;
    
    public JF_Menu() {
        initComponents();
        setLocationRelativeTo(null);
        setExtendedState(MAXIMIZED_BOTH);
    }

    public void setnombre(String nombrecito){
     
        usu=nombrecito;
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu4 = new javax.swing.JMenu();
        jMenu8 = new javax.swing.JMenu();
        jMenuBar3 = new javax.swing.JMenuBar();
        jMenu10 = new javax.swing.JMenu();
        jMenu11 = new javax.swing.JMenu();
        jMenuBar4 = new javax.swing.JMenuBar();
        jMenu12 = new javax.swing.JMenu();
        jMenu13 = new javax.swing.JMenu();
        jMenuBar5 = new javax.swing.JMenuBar();
        jMenu14 = new javax.swing.JMenu();
        jMenu15 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        Menu_Paciente = new javax.swing.JMenu();
        SubMenu_MantenimientoPaciente = new javax.swing.JMenuItem();
        MenuPagos = new javax.swing.JMenu();
        Submenu_Pagos = new javax.swing.JMenuItem();
        MenuCitas = new javax.swing.JMenu();
        jMenuItem2 = new javax.swing.JMenuItem();
        MenuHistorial = new javax.swing.JMenu();
        MenuReportes = new javax.swing.JMenu();
        Menu_ReportesPagos = new javax.swing.JMenuItem();
        Menu_ReportesCitas = new javax.swing.JMenuItem();
        MenuUsuarios = new javax.swing.JMenu();
        MenuCerrarSesion = new javax.swing.JMenu();

        jMenu4.setText("File");
        jMenuBar2.add(jMenu4);

        jMenu8.setText("Edit");
        jMenuBar2.add(jMenu8);

        jMenu10.setText("File");
        jMenuBar3.add(jMenu10);

        jMenu11.setText("Edit");
        jMenuBar3.add(jMenu11);

        jMenu12.setText("File");
        jMenuBar4.add(jMenu12);

        jMenu13.setText("Edit");
        jMenuBar4.add(jMenu13);

        jMenu14.setText("File");
        jMenuBar5.add(jMenu14);

        jMenu15.setText("Edit");
        jMenuBar5.add(jMenu15);

        jMenuItem1.setText("jMenuItem1");

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Bienvenidos a su Clinica Dental");

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/i_citas.png"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel2MousePressed(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/i_salir.png"))); // NOI18N
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel3MousePressed(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/i_usuario.png"))); // NOI18N
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel4MousePressed(evt);
            }
        });

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/i_nuevopaciente.png"))); // NOI18N
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel5MousePressed(evt);
            }
        });

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/i_pagos.png"))); // NOI18N
        jLabel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel6MousePressed(evt);
            }
        });

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/i_directoriopacientes.png"))); // NOI18N
        jLabel7.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel7MousePressed(evt);
            }
        });

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/i_historial3.png"))); // NOI18N
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel8MousePressed(evt);
            }
        });

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Image/i_reportes.png"))); // NOI18N
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel9MousePressed(evt);
            }
        });

        Menu_Paciente.setBackground(new java.awt.Color(0, 0, 204));
        Menu_Paciente.setForeground(new java.awt.Color(0, 0, 204));
        Menu_Paciente.setText("Paciente");
        Menu_Paciente.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N

        SubMenu_MantenimientoPaciente.setText("Mantenimiento Paciente");
        SubMenu_MantenimientoPaciente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SubMenu_MantenimientoPacienteActionPerformed(evt);
            }
        });
        Menu_Paciente.add(SubMenu_MantenimientoPaciente);

        jMenuBar1.add(Menu_Paciente);

        MenuPagos.setBackground(new java.awt.Color(0, 0, 204));
        MenuPagos.setForeground(new java.awt.Color(0, 0, 204));
        MenuPagos.setText("Pagos");
        MenuPagos.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N

        Submenu_Pagos.setText("Realizar Pagos");
        Submenu_Pagos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Submenu_PagosActionPerformed(evt);
            }
        });
        MenuPagos.add(Submenu_Pagos);

        jMenuBar1.add(MenuPagos);

        MenuCitas.setBackground(new java.awt.Color(0, 0, 204));
        MenuCitas.setForeground(new java.awt.Color(0, 0, 204));
        MenuCitas.setText("Citas");
        MenuCitas.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N

        jMenuItem2.setText("Mantenimiento Citas");
        MenuCitas.add(jMenuItem2);

        jMenuBar1.add(MenuCitas);

        MenuHistorial.setBackground(new java.awt.Color(0, 0, 204));
        MenuHistorial.setForeground(new java.awt.Color(0, 0, 204));
        MenuHistorial.setText("Historial Clinico");
        MenuHistorial.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        jMenuBar1.add(MenuHistorial);

        MenuReportes.setBackground(new java.awt.Color(0, 0, 204));
        MenuReportes.setForeground(new java.awt.Color(0, 0, 204));
        MenuReportes.setText("Reportes");
        MenuReportes.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N

        Menu_ReportesPagos.setText("Reportes de Pagos");
        MenuReportes.add(Menu_ReportesPagos);

        Menu_ReportesCitas.setText("Reportes de Citas");
        MenuReportes.add(Menu_ReportesCitas);

        jMenuBar1.add(MenuReportes);

        MenuUsuarios.setBackground(new java.awt.Color(0, 0, 204));
        MenuUsuarios.setForeground(new java.awt.Color(0, 0, 204));
        MenuUsuarios.setText("Usuarios");
        MenuUsuarios.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        MenuUsuarios.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                MenuUsuariosMouseClicked(evt);
            }
        });
        jMenuBar1.add(MenuUsuarios);

        MenuCerrarSesion.setForeground(new java.awt.Color(0, 0, 204));
        MenuCerrarSesion.setText("Cerrar Sesión");
        MenuCerrarSesion.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        MenuCerrarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                MenuCerrarSesionActionPerformed(evt);
            }
        });
        jMenuBar1.add(MenuCerrarSesion);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, 0, Short.MAX_VALUE))
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(52, 52, 52)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(218, 218, 218))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel2))
                .addGap(95, 95, 95)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(283, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MousePressed
        JD_DirectorioPacientes1 obj = new JD_DirectorioPacientes1();
        obj.setVisible(true);
    }//GEN-LAST:event_jLabel2MousePressed

    private void jLabel4MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MousePressed
        JD_DirectorioPacientes1 obj = new JD_DirectorioPacientes1();
        obj.setVisible(true);
    }//GEN-LAST:event_jLabel4MousePressed

    private void jLabel5MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MousePressed
        JD_NuevoPaciente obj = new JD_NuevoPaciente();
        obj.setVisible(true);
    }//GEN-LAST:event_jLabel5MousePressed

    private void jLabel6MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel6MousePressed
        JD_DirectorioPacientes1 obj = new JD_DirectorioPacientes1();
        obj.setVisible(true);
    }//GEN-LAST:event_jLabel6MousePressed

    private void jLabel7MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel7MousePressed
        JD_DirectorioPacientes1 obj = new JD_DirectorioPacientes1();  //DIRECTORIO PACIENTES
        obj.setVisible(true);
    }//GEN-LAST:event_jLabel7MousePressed

    private void jLabel3MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MousePressed
        dispose();
    }//GEN-LAST:event_jLabel3MousePressed

    private void jLabel8MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MousePressed
        JD_DirectorioPacientes1 obj = new JD_DirectorioPacientes1();
        obj.setVisible(true);
    }//GEN-LAST:event_jLabel8MousePressed

    private void jLabel9MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MousePressed
        JD_Reportes obj = new JD_Reportes(this, true);
        obj.setVisible(true);
    }//GEN-LAST:event_jLabel9MousePressed

    private void MenuUsuariosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_MenuUsuariosMouseClicked
       JD_MantenimientoUsuario obj = new JD_MantenimientoUsuario(this, true);
        obj.setVisible(true); 
    }//GEN-LAST:event_MenuUsuariosMouseClicked

    private void Submenu_PagosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Submenu_PagosActionPerformed
        JD_Pagos obj = new JD_Pagos();
        obj.setVisible(true);
    }//GEN-LAST:event_Submenu_PagosActionPerformed

    private void SubMenu_MantenimientoPacienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SubMenu_MantenimientoPacienteActionPerformed
//        JD_NuevoPaciente obj = new JD_NuevoPaciente(new javax.swing.JDialog(), true);
//        obj.setVisible(true);
    }//GEN-LAST:event_SubMenu_MantenimientoPacienteActionPerformed

    private void MenuCerrarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_MenuCerrarSesionActionPerformed
        dispose();
    }//GEN-LAST:event_MenuCerrarSesionActionPerformed

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JF_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JF_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JF_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JF_Menu.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JF_Menu().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu MenuCerrarSesion;
    private javax.swing.JMenu MenuCitas;
    private javax.swing.JMenu MenuHistorial;
    private javax.swing.JMenu MenuPagos;
    private javax.swing.JMenu MenuReportes;
    private javax.swing.JMenu MenuUsuarios;
    private javax.swing.JMenu Menu_Paciente;
    private javax.swing.JMenuItem Menu_ReportesCitas;
    private javax.swing.JMenuItem Menu_ReportesPagos;
    private javax.swing.JMenuItem SubMenu_MantenimientoPaciente;
    private javax.swing.JMenuItem Submenu_Pagos;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JMenu jMenu10;
    private javax.swing.JMenu jMenu11;
    private javax.swing.JMenu jMenu12;
    private javax.swing.JMenu jMenu13;
    private javax.swing.JMenu jMenu14;
    private javax.swing.JMenu jMenu15;
    private javax.swing.JMenu jMenu4;
    private javax.swing.JMenu jMenu8;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenuBar jMenuBar3;
    private javax.swing.JMenuBar jMenuBar4;
    private javax.swing.JMenuBar jMenuBar5;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    // End of variables declaration//GEN-END:variables
}
