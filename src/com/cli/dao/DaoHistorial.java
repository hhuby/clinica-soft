
package com.cli.dao;

import com.cli.beans.Historial;
import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DaoHistorial {
    
    //===================================================
    public static  ArrayList<Historial> listar (){
        ArrayList<Historial> li = new ArrayList<Historial>();
        conexion c = new conexion();
        Connection cn= c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("SELECT * FROM historial ");
//            ps.setInt(1, cod_paci);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                Historial his = new Historial();
                his.setCod_historia(rs.getInt(1));
                his.setCod_paciente(rs.getInt(2));
                his.setIndiccion(rs.getString(3));
                his.setCod_ser(rs.getInt(4));
                his.setCod_doctor(rs.getInt(5));
                his.setFecha(rs.getString(6));
                
                li.add(his);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DaoHistorial.class.getName()).log(Level.SEVERE, null, ex);
        }
       
        return  li ;
        
 
        
        
    }
    
    //=========================================
    
    public static  void insertar(Historial his){
           conexion c = new conexion();
        Connection cn= c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("INSERT INTO historial( cod_paciente, indicaciones, cod_servicio, cod_doctor,HistoriaDientes) VALUES (?,?,?,?,?)");
            ps.setInt(1, his.getCod_paciente());
            ps.setString(2, his.getIndiccion());
            ps.setInt(3, his.getCod_ser());
            ps.setInt(4, his.getCod_doctor());
            ps.setString(5, his.getFecha());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(DaoHistorial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
