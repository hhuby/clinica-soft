package com.cli.dao;

import com.cli.beans.servicios;
import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class dao_servicio {
    
    public  static  int codigo_ser (String servi){
         conexion c = new conexion();
         Connection cn = c.abrir();
         int cod =0;
        try {
            PreparedStatement ps = cn.prepareStatement("select cod_servicio from servicio where descripcion like ? ");
            ps.setString(1, servi);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                cod=rs.getInt("cod_servicio");
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_servicio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  cod;
    }
    
    //=======================================================================00000
      public  static  ArrayList<servicios> listar_servi (){
          ArrayList<servicios> li=new ArrayList<servicios>();
         conexion c = new conexion();
         Connection cn = c.abrir();
         
        try {
            PreparedStatement ps = cn.prepareStatement("select * from Servicio  ");
            
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                servicios ser = new servicios();
                ser.setCod(rs.getInt(1));
                ser.setDescripcion(rs.getString(2));
                li.add(ser);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_servicio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  li;
    }
      
      
      
       public  static  float precio (String servi){
         conexion c = new conexion();
         Connection cn = c.abrir();
         float cod =0;
        try {
            PreparedStatement ps = cn.prepareStatement("select precio from servicio where descripcion like ? ");
            ps.setString(1, servi);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                cod=rs.getFloat("precio");
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_servicio.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  cod;
    }
}
