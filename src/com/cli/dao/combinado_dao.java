
package com.cli.dao;

import com.cli.beans.combinada;
import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class combinado_dao {
   
    public static ArrayList<combinada> listar(String nombredoc,String fecha){
        ArrayList<combinada>lista = new ArrayList<combinada>();
        String sql="select a.Horario,d.ap_pat from Horario a, Doctor b,Cita c,Paciente d where c.cod_paciente=d.cod_paciente and c.cod_tipo_paci=d.cod_tipo_paci and c.cod_Horario=a.cod_Horario and b.cod_doctor=c.cod_doctor and b.nombre = '"+nombredoc+"'  and c.fecha_programada  = '"+fecha+"' ";
        
              
        conexion c=new conexion();
        Connection cn=c.abrir();
        try {
            Statement ps =cn.createStatement();
         
            ResultSet rs = ps.executeQuery(sql);
            while (rs.next()) {                
                combinada bean = new combinada();
                bean.setFecha(rs.getString("Horario"));
                 bean.setNombre_doc(rs.getString("ap_pat"));
                lista.add(bean);
            }
        } catch (SQLException ex) {
            Logger.getLogger(combinado_dao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  lista;
    }
}
