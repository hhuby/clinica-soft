/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cli.dao;

import com.cli.beans.Doctor;
import com.cli.beans.HorarioCita;
import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author HECTOR
 */
public class dao_horarioCita {
    public static ArrayList<HorarioCita>lista_hc(){
      ArrayList<HorarioCita>lis=new ArrayList<HorarioCita>();
      conexion c = new conexion();
      Connection cn = c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("select * from Horario_Cita");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                HorarioCita hc=new HorarioCita();
                hc.setId(rs.getInt(1));
                hc.setHora(rs.getString(2));
               
                lis.add(hc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
      return  lis;
  }
}
