
package com.cli.dao;

import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class dao_sistema {
    
    public  static  String dia(){
        String dia =null;
        conexion c = new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("select DAY(GETDATE())");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
               dia=rs.getString(1); 
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_sistema.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  dia;
    }
    
    //=========================================================
    public  static  String mes(){
        String mes =null;
        conexion c = new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("select month(GETDATE())");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
               mes=rs.getString(1); 
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_sistema.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  mes;
    }
    
    //=======================================================
    
    public  static  String año(){
        String año =null;
        conexion c = new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("select year(GETDATE())");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
               año=rs.getString(1); 
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_sistema.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  año;
    }
}
