
package com.cli.dao;

import com.cli.beans.Cita;
import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import sun.org.mozilla.javascript.internal.regexp.SubString;

public class dao_cita {
    private static String generar_cod(){
        String cod="";
        conexion c = new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("SELECT COUNT(1) FROM  `cita`");
            ResultSet rs=ps.executeQuery();
            int i=rs.getInt(1);
            cod="000"+i;
            cod.substring(i, i);
            
        } catch (SQLException ex) {
            System.out.println("Error dao_cita.dao_cita: "+ex.getMessage());
        }
        return cod;
    }
    public static void agragr_cita(Cita cita){
        conexion c = new conexion();
        Connection cn=c.abrir();
        String sql="insert into cita ("
                + "cod_doctor,"
                + "cod_paciente,"
                + "fecha_programada,"
                + "observacion,"
                + "estado,"
                + "cod_servicio,"
                + "cod_tipo_paci,"
                + "cod_Horario) "
                + "values(?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps = cn.prepareStatement("");
            ps.setInt(1, cita.getCod_doctor());
            ps.setInt(2, cita.getCod_paciente());
            ps.setString(3, cita.getFecha_progra());
            ps.setString(4, cita.getObservacion());
            ps.setString(5, cita.getEstado());
            ps.setInt(6, cita.getCod_serv());
            ps.setInt(7, cita.getCod_tipo());
            ps.setInt(8, cita.getCod_hora());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dao_cita.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void modificar_cita(Cita cita){
        conexion c = new conexion();
        Connection cn=c.abrir();
        String sql="UPDATE "
                + "`cita` "
                + "SET "
                + "`fecha_programada`=?,`cod_horario`=? "
                + "WHERE "
                + "`cod_doctor`=? AND `cod_paciente`=? AND `cod_cita`=?";
        try {
            PreparedStatement ps = cn.prepareStatement(sql);
            ps.setString(1, cita.getFecha_progra());
            ps.setInt(2, obtener_codigo_citas(cita.getHora()));  
            ps.setInt(3, cita.getCod_doctor());
            ps.setInt(4, cita.getCod_paciente());
            ps.setString(5, cita.getCod_cita());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dao_cita.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     private static int obtener_codigo_citas(String hora){
         conexion c = new conexion();
        int h=0;
        Connection cn=c.abrir();
        String sql="";
        try {
            sql="SELECT cod_horario from horario_cita where hora=?";
            PreparedStatement ps = cn.prepareStatement(sql);
            ps.setString(1, hora);       
            ResultSet rs=ps.executeQuery();     
            if(rs.next()){
                h=rs.getInt("cod_horario");  
            }  
        } catch (SQLException ex) {
            System.out.println("Error dao_cita.extraer_cita_doctor: "+ex.getMessage());;
        }
        return h;
    }
    public static void eliminar_cita(Cita cita){
        conexion c = new conexion();
        Connection cn=c.abrir();
        String sql="UPDATE "
                + "`cita` "
                + "SET "
                + "`estado`=0 "
                + "WHERE "
                + "`cod_doctor`=? AND `cod_paciente`=? AND `cod_cita`=? AND `cod_horario`=? ";
        try {
            PreparedStatement ps = cn.prepareStatement(sql);
            ps.setInt(1, cita.getCod_doctor());
            ps.setInt(2, cita.getCod_paciente());
            ps.setString(3, cita.getCod_cita());
            ps.setInt(4, obtener_codigo_citas(cita.getHora()));
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dao_cita.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static ArrayList<Cita> extraer_cita_doctor(int cod_doctor,String fecha){
         conexion c = new conexion();
         ArrayList<Cita> citas=null;
        Connection cn=c.abrir();
        String sql="";
        try {
            sql="SELECT "
                    + "c.cod_cita as cod_cita, "
                    + "p.cod_paciente as cod_pac, "
                    + "concat(p.nombre,' ',p.ap_pat,' ',p.ap_mat) as nom_pac, "
                    + "c.cod_doctor as cod_doc, "
                    + "concat(d.nombre,' ',d.ap_pat,' ',d.ap_mat) as nom_doc, "
                    + "h.hora as hora, "
                    + "c.fecha_programada as fecha,"
                    + "c.estado as estado "
                    + "FROM cita c, doctor d, paciente p,horario_cita h "
                    + "WHERE d.cod_doctor = c.cod_doctor "
                    + "AND c.fecha_programada =? "
                    + "AND d.cod_doctor=? AND p.cod_paciente=c.cod_paciente AND c.cod_horario=h.cod_horario";
            PreparedStatement ps = cn.prepareStatement(sql);
            ps.setString(1, fecha);
            ps.setInt(2, cod_doctor);
            ResultSet rs=ps.executeQuery();
            citas=new ArrayList<Cita>();
            while(rs.next()){
                Cita cita=new Cita();
                cita.setCod_cita(rs.getString("cod_cita"));
                cita.setCod_paciente(rs.getInt("cod_pac"));
                cita.setNom_com_pac(rs.getString("nom_pac"));
                cita.setCod_doctor(rs.getInt("cod_doc"));
                cita.setNom_com_doc(rs.getString("nom_doc"));
                cita.setHora(rs.getString("hora"));
                cita.setFecha_progra(rs.getString("fecha"));
                cita.setEstado(rs.getString("estado"));
                citas.add(cita);
            }
            
        } catch (SQLException ex) {
            System.out.println("Error dao_cita.extraer_cita_doctor: "+ex.getMessage());;
        }
        return citas;
    }
}
