
package com.cli.dao;

import com.cli.beans.Doctor;
import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class dao_doctor {
  public static ArrayList<Doctor>lista_doc(){
      ArrayList<Doctor>lis=new ArrayList<Doctor>();
      conexion c = new conexion();
      Connection cn = c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("select * from doctor");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                Doctor doc=new Doctor();
                doc.setCod_doctor(rs.getInt(1));
                doc.setNombre(rs.getString(2));
                doc.setAp_paterno(rs.getString(3));
                doc.setApe_materno(rs.getString(4));
                doc.setDni(rs.getString(5));
                doc.setTelefono(rs.getString(6));
                lis.add(doc);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
      return  lis;
  }
  
  ////////////////////////////////////////////////////////////////////////////
    public static int buscar_id_doc(String nomdoc){
     int id =0;
      conexion c = new conexion();
      Connection cn = c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("select cod_doctor from doctor where nombre=?");
            ps.setString(1, nomdoc);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
              id=rs.getInt("cod_doctor");
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_doctor.class.getName()).log(Level.SEVERE, null, ex);
        }
      return  id;
  } 
}
