
package com.cli.dao;

import com.cli.beans.mezcla;
import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class dao_mezcla {
    
    public static ArrayList<mezcla>lista(int cod){
        ArrayList<mezcla>li=new ArrayList<mezcla>();
        conexion c = new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps =cn.prepareStatement("select c.fecha_programada , d.nombre ,a.descripcion  from Servicio a , Paciente b , Cita c , Doctor d where a.cod_servicio=c.cod_servicio and b.cod_paciente=c.cod_paciente and d.cod_doctor=c.cod_doctor and b.cod_paciente=?");
            ps.setInt(1, cod);
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {                
            mezcla me = new mezcla();
            me.setFecha(rs.getString("fecha_programada"));
            me.setNombre_doc(rs.getString("nombre"));
            me.setTratamiento(rs.getString("descripcion"));
            li.add(me);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_mezcla.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return  li ;
    }
}
