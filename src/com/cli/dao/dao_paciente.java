package com.cli.dao;

import com.cli.beans.Paciente;
import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class dao_paciente {
        public static ArrayList<Paciente> buscar_paciente_array(String apepat){
        ArrayList<Paciente> lista = new ArrayList<Paciente>();
            Paciente pa=null;
           conexion con = new conexion();
            Connection c = con.abrir();
        try {
            Statement st=c.createStatement();
           
            ResultSet rs =st.executeQuery("select *from Paciente where ap_pat like '"+apepat+"%'");
            while (rs.next()) {                
             pa=new Paciente();
             pa.setCod_paciente(rs.getInt(1));
             
             pa.setNombre(rs.getString(2));
             pa.setApe_paterno(rs.getString(3));
             pa.setApe_materno(rs.getString(4));
             pa.setFech_nacimin(rs.getString(5));
             pa.setEstado(rs.getString(6));
             pa.setGenero(rs.getString(7));
       
             
             pa.setDireccion(rs.getString(8));
             pa.setTelefono(rs.getString(9));
             pa.setDni(rs.getString(10));
             pa.setFecha_ingreso(rs.getString(11));
             pa.setTipo_paciente(rs.getInt(12));
             pa.setEdad(rs.getInt(13));
           
             lista.add(pa);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_paciente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }
    
        //======================================================
    
    public static Paciente buscar_paciente(int cod){
        Paciente pa=null;
           conexion con = new conexion();
            Connection c = con.abrir();
        try {
            PreparedStatement ps= c.prepareStatement("select * from paciente where cod_paciente=? ");
            ps.setInt(1, cod);
            ResultSet rs =ps.executeQuery();
            while (rs.next()) {                
               pa=new Paciente();
             pa.setTipo_paciente(rs.getInt(13)); 
             pa.setNombre(rs.getString(2));
             pa.setApe_paterno(rs.getString(3));
             pa.setApe_materno(rs.getString(4));
             pa.setFech_nacimin(rs.getString(5));
             pa.setEstado(rs.getString(6));
             pa.setGenero(rs.getString(7));
             pa.setDireccion(rs.getString(8));
             pa.setTelefono(rs.getString(9));
             pa.setDni(rs.getString(10));
             pa.setFecha_ingreso(rs.getString(11));
             pa.setEdad(rs.getInt(14));
             pa.setCod_paciente(rs.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_paciente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pa;
    }
    //==============================================================
    
    public static void agregar_cliente(Paciente p){
           conexion con = new conexion();
            Connection c = con.abrir();
           
        try {
         
            PreparedStatement ps = c.prepareStatement("insert into Paciente (nombre,ap_pat,ap_mat,fecha_nacimiento,genero,direccion,telefono,dni,fecha_ingreso,cod_tipo_paci,edad,estado) values (?,?,?,?,?,?,?,?,?,?,?,?)");
         
            ps.setString(1, p.getNombre());
            ps.setString(2, p.getApe_paterno());
            ps.setString(3, p.getApe_materno());
            ps.setString(4, p.getFech_nacimin());
            ps.setString(5, p.getGenero());
            ps.setString(6, p.getDireccion());
            ps.setString(7, p.getTelefono());
            ps.setString(8, p.getDni());
            ps.setString(9, p.getFecha_ingreso());
              ps.setInt(10, p.getTipo_paciente());
              ps.setInt(11, p.getEdad());
       
          ps.setString(12, p.getEstado());
            ps.executeUpdate();
                        
            
        } catch (SQLException ex) {
            Logger.getLogger(dao_paciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
     public static void agregar_editar(Paciente p){
           conexion con = new conexion();
            Connection c = con.abrir();
          
        try {
         
            PreparedStatement ps = c.prepareStatement("update Paciente set nombre=?,ap_pat=?,ap_mat=?,fecha_nacimiento =?,genero=?,direccion=?,telefono=?,dni=?,fecha_ingreso=?,edad=?,estado=?,cod_tipo_paci=? where cod_paciente=?");
            
               ps.setInt(13, p.getCod_paciente());
               ps.setInt(12, p.getCod_paciente());
            ps.setString(1, p.getNombre());
            ps.setString(2, p.getApe_paterno());
            ps.setString(3, p.getApe_materno());
            ps.setString(4, p.getFech_nacimin());
            ps.setString(5, p.getGenero());
            ps.setString(6, p.getDireccion());
            ps.setString(7, p.getTelefono());
            ps.setString(8, p.getDni());
            ps.setString(9, p.getFecha_ingreso());
           
              ps.setInt(10, p.getEdad());
              ps.setString(11, p.getEstado());
            ps.executeUpdate();
                        
            
        } catch (SQLException ex) {
            Logger.getLogger(dao_paciente.class.getName()).log(Level.SEVERE, null, ex);
        }
    
    }
    
     
     //====================000000000000
       public static String buscar_pacientenom(int cod){
        String pa=null;
           conexion con = new conexion();
            Connection c = con.abrir();
        try {
            PreparedStatement ps= c.prepareStatement("select nombre from paciente where cod_paciente=? ");
            ps.setInt(1, cod);
            ResultSet rs =ps.executeQuery();
            while (rs.next()) {                
               pa=rs.getString("nombre");
            
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_paciente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pa;
    }
       
       //========================================================
       
       public static  int edad (String fecha){
           int edad =0;
              conexion con = new conexion();
            Connection c = con.abrir();
        try {
            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery("select YEAR(GETDATE())-YEAR('"+fecha+"')");
            while (rs.next()) {                
                edad=rs.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_paciente.class.getName()).log(Level.SEVERE, null, ex);
        }
          return  edad ;
       }
       
       //========================================
         public static int buscar_cod(String nom){
        int pa=0;
           conexion con = new conexion();
            Connection c = con.abrir();
        try {
            PreparedStatement ps= c.prepareStatement("select cod_paciente from paciente where  nombre=? ");
            ps.setString(1, nom);
            ResultSet rs =ps.executeQuery();
            while (rs.next()) {                
               pa=rs.getInt("cod_paciente");
            
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_paciente.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pa;
    }
}
