package com.cli.dao;
import com.cli.beans.Usuario;
import com.cli.util.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;


public class dao_usuario {
    public static  String validar(Usuario usuario){
        String valor=null;
        conexion c= new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps=cn.prepareStatement("select nombre from usuario where nickname=? and password=? ");
            ps.setString(1, usuario.getNick_name());
            ps.setString(2, usuario.getPassword());
            
            ResultSet rs=ps.executeQuery();
            while (rs.next()) {                
                valor=rs.getString("nombre");
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  valor;
    }
    //==============================================
    
    public static  String nuevo_usuario( Usuario usuario){
        String men="se agrego el usuario correctamanete";
         conexion c= new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("insert into usuario (nombre,ap_pat,ap_mat,nickname,estado,contraseña,cargo,dni) values (?,?,?,?,?,?,?,?)" );
            ps.setString(1, usuario.getNombre());
            ps.setString(2, usuario.getApe_paterno());
            ps.setString(3, usuario.getApe_materno());
            ps.setString(4, usuario.getNick_name());
            ps.setString(5, usuario.getEstado());
            ps.setString(6, usuario.getPassword());
            ps.setString(7, usuario.getCargo());
            ps.setString(8, usuario.getDni());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            men="error en el mensaje"+ex.getMessage();
        }
        return  men ;
    }
    
    //======================================================
    
    public static  String eliminar(String nombre){
        String mensaje="se elimino correctamente";
          conexion c= new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("delete from usuario where nombre=?");
            ps.setString(1, nombre);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(dao_usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        return  mensaje;
    }
    
    //=============================================
    
    public static  ArrayList<Usuario>listar(){
        ArrayList<Usuario> li =new ArrayList<Usuario>();
        conexion c= new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps =cn.prepareStatement("select * from usuario");
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                Usuario usu = new Usuario();
                usu.setCod_usuario(rs.getInt(1));
                usu.setNombre(rs.getString(2));
                usu.setApe_paterno(rs.getString(3));
                usu.setApe_materno(rs.getString(4));
                usu.setNick_name(rs.getString(5));
                usu.setPassword(rs.getString(6));
                usu.setEstado(rs.getString(7));
                usu.setDni(rs.getString(8));
                usu.setCargo(rs.getString(9));
                li.add(usu);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return li;
    }
    
    //===========================================
     public static  Usuario buscar(String nombre){
        Usuario usu =new Usuario();
        conexion c= new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps =cn.prepareStatement("select * from usuario where nombre=?");
            ps.setString(1, nombre);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {                
                 usu = new Usuario();
           
                usu.setNombre(rs.getString(2));
                usu.setApe_paterno(rs.getString(3));
                usu.setApe_materno(rs.getString(4));
                usu.setNick_name(rs.getString(5));
                usu.setPassword(rs.getString(6));
                
                usu.setDni(rs.getString(8));
                
          
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(dao_usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return usu;
    }
    //============================000000000000000000000000000000000000000
     
      public static  String editar_usuario( Usuario usuario){
        String men="se edito los  correctamanete";
         conexion c= new conexion();
        Connection cn=c.abrir();
        try {
            PreparedStatement ps = cn.prepareStatement("update usuario set ap_pat=?,ap_mat=?,nickname=?,contraseña=?,cargo=?,dni=? where nombre =?" );
       
            ps.setString(1, usuario.getApe_paterno());
            ps.setString(2, usuario.getApe_materno());
            ps.setString(3, usuario.getNick_name());
     
            ps.setString(4, usuario.getPassword());
            ps.setString(5, usuario.getCargo());
            ps.setString(6, usuario.getDni());
            ps.setString(7, usuario.getNombre());
            
            ps.executeUpdate();
        } catch (SQLException ex) {
            men="error en el mensaje"+ex.getMessage();
        }
        return  men ;
    }
}
