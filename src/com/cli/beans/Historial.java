
package com.cli.beans;


public class Historial {
    private int cod_historia,cod_doctor,cod_paciente,cod_ser;
    private String indiccion, fecha;

    public int getCod_doctor() {
        return cod_doctor;
    }

    public void setCod_doctor(int cod_doctor) {
        this.cod_doctor = cod_doctor;
    }

    public int getCod_historia() {
        return cod_historia;
    }

    public void setCod_historia(int cod_historia) {
        this.cod_historia = cod_historia;
    }

    public int getCod_paciente() {
        return cod_paciente;
    }

    public void setCod_paciente(int cod_paciente) {
        this.cod_paciente = cod_paciente;
    }

    public int getCod_ser() {
        return cod_ser;
    }

    public void setCod_ser(int cod_ser) {
        this.cod_ser = cod_ser;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getIndiccion() {
        return indiccion;
    }

    public void setIndiccion(String indiccion) {
        this.indiccion = indiccion;
    }
    
}
