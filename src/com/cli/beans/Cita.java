
package com.cli.beans;
public class Cita {
   private int cod_doctor,cod_paciente,cod_tipo ,cod_serv,cod_hora;
   private String fecha_progra,cod_cita,observacion,estado,hora,nom_com_doc,nom_com_pac;

   

    public String getCod_cita() {
        return cod_cita;
    }

    public void setCod_cita(String cod_cita) {
        this.cod_cita = cod_cita;
    }

    public int getCod_doctor() {
        return cod_doctor;
    }

    public void setCod_doctor(int cod_doctor) {
        this.cod_doctor = cod_doctor;
    }

    public int getCod_hora() {
        return cod_hora;
    }

    public void setCod_hora(int cod_hora) {
        this.cod_hora = cod_hora;
    }

    public int getCod_paciente() {
        return cod_paciente;
    }

    public void setCod_paciente(int cod_paciente) {
        this.cod_paciente = cod_paciente;
    }

    public int getCod_serv() {
        return cod_serv;
    }

    public void setCod_serv(int cod_serv) {
        this.cod_serv = cod_serv;
    }

    public int getCod_tipo() {
        return cod_tipo;
    }

    public void setCod_tipo(int cod_tipo) {
        this.cod_tipo = cod_tipo;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getFecha_progra() {
        return fecha_progra;
    }

    public void setFecha_progra(String fecha_progra) {
        this.fecha_progra = fecha_progra;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public String getNom_com_doc() {
        return nom_com_doc;
    }

    public void setNom_com_doc(String nom_com_doc) {
        this.nom_com_doc = nom_com_doc;
    }

    public String getNom_com_pac() {
        return nom_com_pac;
    }

    public void setNom_com_pac(String nom_com_pac) {
        this.nom_com_pac = nom_com_pac;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
   
   
   
}
