package com.cli.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;


public class conexion {
    
    
    public  Connection cn;
    public Connection abrir() {
       
         Connection cn=null;
    String url="jdbc:mysql://localhost/clinica_final";
    String usuario="root";
    String Password="";
        try {
            Class.forName("com.mysql.jdbc.Driver");
            cn=DriverManager.getConnection(url, usuario, Password);
       
        } catch (Exception ex) {
            System.out.println("Error en la conexion a la Bd:"+ex);
        }
        return  cn;
    }
    public void EjecutarReporte(String URL)
 {
        try {            
            JasperReport Jas_Rep= JasperCompileManager.compileReport(URL);
            JasperPrint Jas_Prin= JasperFillManager.fillReport(Jas_Rep, null,this.abrir());
            JasperViewer.viewReport(Jas_Prin,false);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,e);
        }
 }
    

   
}
