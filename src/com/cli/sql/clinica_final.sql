-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 18-08-2013 a las 23:21:46
-- Versión del servidor: 5.6.12-log
-- Versión de PHP: 5.4.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `clinica_final`
--
CREATE DATABASE IF NOT EXISTS `clinica_final` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `clinica_final`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cita`
--

CREATE TABLE IF NOT EXISTS `cita` (
  `cod_horario` int(11) NOT NULL,
  `cod_doctor` int(11) NOT NULL,
  `cod_paciente` int(11) NOT NULL,
  `cod_cita` varchar(7) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `fecha_programada` date DEFAULT NULL,
  `observacion` varchar(100) DEFAULT NULL,
  `estado` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_horario`,`cod_doctor`,`cod_paciente`),
  KEY `fk_Cita_Horario_Cita1_idx` (`cod_horario`),
  KEY `fk_Cita_Doctor1_idx` (`cod_doctor`),
  KEY `fk_Cita_Paciente1_idx` (`cod_paciente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cita`
--

INSERT INTO `cita` (`cod_horario`, `cod_doctor`, `cod_paciente`, `cod_cita`, `fecha_registro`, `fecha_programada`, `observacion`, `estado`) VALUES
(1, 1, 1, 'CI-0001', '2013-07-11', '2013-07-26', 'ninguna', '1'),
(2, 1, 2, 'CI-0002', '2013-07-03', '2013-07-03', 'ninguna', '1'),
(9, 2, 3, 'CI-0003', '2013-07-17', '2013-07-10', 'ninguna', '1'),
(13, 1, 3, 'CI-0004', '2013-07-16', '2013-07-16', 'ninguna', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contrato_pago`
--

CREATE TABLE IF NOT EXISTS `contrato_pago` (
  `cod_contrato` int(11) NOT NULL AUTO_INCREMENT,
  `cod_paciente` int(11) NOT NULL,
  `num_cuotas` int(11) DEFAULT NULL,
  `monto_inicial` double(10,2) DEFAULT NULL,
  `monto_total` double(10,2) DEFAULT NULL,
  `deuda` double(10,2) DEFAULT NULL,
  `estado` varchar(1) DEFAULT NULL,
  PRIMARY KEY (`cod_contrato`),
  KEY `fk_Contrato_Pago_Paciente1_idx` (`cod_paciente`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `contrato_pago`
--

INSERT INTO `contrato_pago` (`cod_contrato`, `cod_paciente`, `num_cuotas`, `monto_inicial`, `monto_total`, `deuda`, `estado`) VALUES
(1, 1, 4, 100.00, 200.00, 30.00, '1'),
(2, 3, 2, 100.00, 400.00, 220.00, '1'),
(3, 3, 4, 50.00, 300.00, 180.00, '1'),
(4, 4, 4, 20.00, 100.00, 10.00, '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doctor`
--

CREATE TABLE IF NOT EXISTS `doctor` (
  `cod_doctor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `ap_pat` varchar(50) DEFAULT NULL,
  `ap_mat` varchar(50) DEFAULT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `telefono` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`cod_doctor`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `doctor`
--

INSERT INTO `doctor` (`cod_doctor`, `nombre`, `ap_pat`, `ap_mat`, `dni`, `telefono`) VALUES
(1, 'darwin', 'robles', 'mendoza', '12345678', '012896585'),
(2, 'cristian', 'fajardo', 'gomero', '12345678', '12345678');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE IF NOT EXISTS `historial` (
  `cod_historial` int(11) NOT NULL AUTO_INCREMENT,
  `cod_paciente` int(11) NOT NULL,
  `indicaciones` varchar(100) DEFAULT NULL,
  `cod_servicio` int(11) NOT NULL,
  `cod_doctor` int(11) NOT NULL,
  PRIMARY KEY (`cod_historial`,`cod_paciente`),
  KEY `fk_Historial_Servicio_idx` (`cod_servicio`),
  KEY `fk_Historial_Doctor1_idx` (`cod_doctor`),
  KEY `fk_Historial_Paciente1_idx` (`cod_paciente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horario_cita`
--

CREATE TABLE IF NOT EXISTS `horario_cita` (
  `cod_horario` int(11) NOT NULL AUTO_INCREMENT,
  `hora` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cod_horario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

--
-- Volcado de datos para la tabla `horario_cita`
--

INSERT INTO `horario_cita` (`cod_horario`, `hora`) VALUES
(1, '9:00'),
(2, '9:30'),
(3, '10:00'),
(4, '10:30'),
(5, '11:00'),
(6, '11:30'),
(7, '12:00'),
(8, '12:30'),
(9, '14:00'),
(10, '14:30'),
(11, '15:00'),
(12, '15:30'),
(13, '16:00'),
(14, '16:30'),
(15, '17:00'),
(16, '17;30'),
(17, '18:00'),
(18, '18:30'),
(19, '19:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paciente`
--

CREATE TABLE IF NOT EXISTS `paciente` (
  `cod_paciente` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `ap_pat` varchar(50) DEFAULT NULL,
  `ap_mat` varchar(50) DEFAULT NULL,
  `fecha_nacimiento` date DEFAULT NULL,
  `estado` varchar(1) DEFAULT NULL,
  `genero` varchar(1) DEFAULT NULL,
  `direccion` varchar(100) DEFAULT NULL,
  `telefono` varchar(16) DEFAULT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `fecha_ingreso` date DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT NULL,
  `cod_tipo_paci` int(11) NOT NULL,
  PRIMARY KEY (`cod_paciente`),
  KEY `fk_Paciente_tipo_paciente1_idx` (`cod_tipo_paci`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `paciente`
--

INSERT INTO `paciente` (`cod_paciente`, `nombre`, `ap_pat`, `ap_mat`, `fecha_nacimiento`, `estado`, `genero`, `direccion`, `telefono`, `dni`, `fecha_ingreso`, `fecha_modificacion`, `cod_tipo_paci`) VALUES
(1, 'roberto', 'mendoza', 'mendoza', '2013-06-14', '1', 'M', 'villa el salvador', '012580458', '12345678', '2013-07-12', '2013-07-24 18:52:40', 1),
(2, 'sergio', 'alcantara', 'medoza', '1990-12-12', '1', 'M', 'chorrillos', '123456789', '12345678', '2013-07-18', '2013-07-24 18:53:39', 1),
(3, 'manuel', 'ramirez ', 'gonzales', '1900-12-25', '1', 'M', 'san juan de miraflores', '123456789', '12345678', '2013-07-05', '2013-07-24 18:54:50', 2),
(4, 'patricia', 'caballero', 'falcon', '1990-06-06', '1', 'F', 'villa maria del triunfo', '123456789', '12345678', '2013-07-31', '2013-07-24 18:55:49', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recibo`
--

CREATE TABLE IF NOT EXISTS `recibo` (
  `cod_recibo` int(11) NOT NULL AUTO_INCREMENT,
  `concepto_recibo` varchar(45) DEFAULT NULL,
  `monto` double(10,2) DEFAULT NULL,
  `fecha_emision` date DEFAULT NULL,
  `estado` varchar(1) DEFAULT NULL,
  `cod_contrato` int(11) NOT NULL,
  PRIMARY KEY (`cod_recibo`),
  KEY `fk_Recibo_Contrato_Pago1_idx` (`cod_contrato`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Volcado de datos para la tabla `recibo`
--

INSERT INTO `recibo` (`cod_recibo`, `concepto_recibo`, `monto`, `fecha_emision`, `estado`, `cod_contrato`) VALUES
(1, 'pago tratamiento', 50.00, '2013-07-27', '1', 1),
(2, 'pago tratamiento', 50.00, '2013-07-10', '1', 2),
(3, 'pago por servicios', 50.00, '2013-07-04', '1', 3),
(4, 'pago por servicios', 50.00, '2013-07-10', '1', 4),
(5, 'pago de servicio', 20.00, '2013-07-24', '1', 1),
(6, 'pago tratamiento', 20.00, '2013-07-02', '1', 2),
(7, 'pago servicio', 20.00, '2013-07-24', '1', 3),
(8, 'pago servicio', 20.00, '2013-07-16', '1', 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE IF NOT EXISTS `servicio` (
  `cod_servicio` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(45) DEFAULT NULL,
  `precio` double(10,2) DEFAULT NULL,
  PRIMARY KEY (`cod_servicio`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`cod_servicio`, `descripcion`, `precio`) VALUES
(1, 'Amalgamas', 300.00),
(2, 'Luz Halogena', 500.00),
(3, 'Endodoncia', 200.00),
(4, 'Periodoncia', 100.00),
(5, 'Ortodoncia', 2000.00),
(6, 'Protesis Fija', 350.00),
(7, 'Perno', 200.00),
(8, 'Reparaciones', 100.00),
(9, 'Cirugía Bucal', 300.00),
(10, 'Blanqueamiento Dental', 200.00),
(11, 'Rayos X', 50.00),
(12, 'Implantes', 200.00),
(13, 'Fluorizacion', 30.00),
(14, 'Sellantes de Fosas', 70.00),
(15, 'Otros', 100.00);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_paciente`
--

CREATE TABLE IF NOT EXISTS `tipo_paciente` (
  `cod_tipo_paci` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_paciente` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`cod_tipo_paci`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipo_paciente`
--

INSERT INTO `tipo_paciente` (`cod_tipo_paci`, `tipo_paciente`) VALUES
(1, 'ADULTO'),
(2, 'NIÑO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `cod_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) DEFAULT NULL,
  `ap_pat` varchar(50) DEFAULT NULL,
  `ap_mat` varchar(50) DEFAULT NULL,
  `nickname` varchar(25) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `dni` varchar(8) DEFAULT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  `fecha_registro` datetime DEFAULT NULL,
  `fecha_modificacion` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`cod_usuario`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`cod_usuario`, `nombre`, `ap_pat`, `ap_mat`, `nickname`, `password`, `estado`, `dni`, `cargo`, `fecha_registro`, `fecha_modificacion`) VALUES
(1, 'Giuliana', 'Corilla', 'Gomero', 'gcorilla', 'e10adc3949ba59abbe56e057f20f883e', '1', '12345678', 'soporte', '2013-07-24 01:16:10', '2013-08-18 23:20:38'),
(2, 'Hector', 'Huby', 'Bautista', 'h.huby', 'e10adc3949ba59abbe56e057f20f883e', '1', '12345678', 'soporte2', '2013-07-24 01:17:03', '2013-07-24 11:17:03');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cita`
--
ALTER TABLE `cita`
  ADD CONSTRAINT `fk_Cita_Doctor1` FOREIGN KEY (`cod_doctor`) REFERENCES `doctor` (`cod_doctor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Cita_Horario_Cita1` FOREIGN KEY (`cod_horario`) REFERENCES `horario_cita` (`cod_horario`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Cita_Paciente1` FOREIGN KEY (`cod_paciente`) REFERENCES `paciente` (`cod_paciente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `contrato_pago`
--
ALTER TABLE `contrato_pago`
  ADD CONSTRAINT `fk_Contrato_Pago_Paciente1` FOREIGN KEY (`cod_paciente`) REFERENCES `paciente` (`cod_paciente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `historial`
--
ALTER TABLE `historial`
  ADD CONSTRAINT `fk_Historial_Doctor1` FOREIGN KEY (`cod_doctor`) REFERENCES `doctor` (`cod_doctor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Historial_Paciente1` FOREIGN KEY (`cod_paciente`) REFERENCES `paciente` (`cod_paciente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_Historial_Servicio` FOREIGN KEY (`cod_servicio`) REFERENCES `servicio` (`cod_servicio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `paciente`
--
ALTER TABLE `paciente`
  ADD CONSTRAINT `fk_Paciente_tipo_paciente1` FOREIGN KEY (`cod_tipo_paci`) REFERENCES `tipo_paciente` (`cod_tipo_paci`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `recibo`
--
ALTER TABLE `recibo`
  ADD CONSTRAINT `fk_Recibo_Contrato_Pago1` FOREIGN KEY (`cod_contrato`) REFERENCES `contrato_pago` (`cod_contrato`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
